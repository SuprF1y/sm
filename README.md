# README #



### Installation ###

* git clone https://SuprF1y@bitbucket.org/SuprF1y/sm.git
* composer install
* config is done by env vars either set in .env or via shell variables 

### Run ###
./switch movie:recommend <genre> <time>

e.g.


./switch movie:recommend animation 12:00

### Test ###
vendor/bin/phpunit --bootstrap ./bootstrap.php --configuration test/phpunit.xml


### Design decisions ###
The timezone is ignored



