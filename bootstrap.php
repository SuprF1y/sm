<?php
/**
 * Author: willem.j.homan@gmail.com
 * Date: 13/09/17
 * Time: 10:56 AM
 */

// load variables from .env into environment
$env = new Dotenv\Dotenv(__DIR__);
try{
    $env->load();
}catch(\Exception $e){
    // eat exception as .env file is not required, can also use env vars
}

// initialise the DI Container
$containerBuilder = new DI\ContainerBuilder();
//$containerBuilder->addDefinitions(['genre' => 'Comedy']);
$containerBuilder->addDefinitions(__DIR__ . '/src/SwitchMedia/Movie/services.php');
$containerBuilder->addDefinitions(__DIR__ . '/src/SwitchMedia/Movie/Recommendation/services.php');
$container = $containerBuilder->build();


require_once (__DIR__ . "/test/SampleData.php");
