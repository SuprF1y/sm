FROM php:7.1-fpm
RUN apt-get update -y && apt-get install -y zip unzip zlib1g-dev git
RUN docker-php-ext-install zip mbstring

#composer
RUN php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
RUN php composer-setup.php
RUN php -r "unlink('composer-setup.php');"
RUN mv composer.phar /usr/local/bin/composer


# xdebug
RUN pecl install xdebug
COPY ./xdebug.ini /usr/local/etc/php/conf.d/

#user
COPY ./.bashrc /root
RUN groupadd -g 1001 interview
RUN useradd -u 1001 -g 1001 -G sudo interview
RUN mkdir /home/interview
RUN chown interview:interview /home/interview


##interview homedir
USER interview
ENV PATH "${PATH}:/home/interview/.composer/vendor/bin"
RUN mkdir /home/interview/app
RUN mkdir /home/interview/.composer


USER root
#RUN ln -s /home/interview/app /var/www/widgets

USER interview
WORKDIR /home/interview/app
# composer dependencies
#RUN composer require phpunit/phpunit
#RUN composer require guzzlehttp/guzzle

#RUN chown www-data:www-data /var/www/interview
