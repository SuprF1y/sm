<?php
namespace Integration\SwitchMedia\Movie;
use GuzzleHttp\Client;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\Psr7\Response;
use SwitchMedia\Movie\Movie;
use SwitchMedia\Movie\Recommendation\RecommendationService;

/**
 * Author: willem.j.homan@gmail.com
 * Date: 14/09/17
 * Time: 4:25 PM
 */
class RecommendationServiceTest extends \PHPUnit\Framework\TestCase
{
    public function testMultiple()
    {
        global $container;
        $container->set('genre', 'comedy');
        $container->set('time', '12:00');
        // Create a mock and queue a response.
        $mockHandler = new MockHandler([
            new Response(200, ['Content-Type:application/json'], \SampleData::asJson())
        ]);
        $container->set(ClientInterface::class, new Client(['handler' => $mockHandler]));
        $recommendationService = $container->make(RecommendationService::class);
        $recommendations = $recommendationService->recommend();
        $expected =  [
            new Movie( "Zootopia", [new \DateTime('19:00:00')], 92),
            new Movie( "Shaun The Sheep", [new \DateTime('19:00:00')], 80)
        ];
        $this->assertEquals($expected, $recommendations);
    }
}
