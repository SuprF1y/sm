<?php
namespace Unit\SwitchMedia\Movie\Recommendation;
use PHPUnit\Framework\TestCase;
use SwitchMedia\Movie\Recommendation\GenreFilter;
use SwitchMedia\Movie\Recommendation\ShowingTransformer;

/**
 * Author: willem.j.homan@gmail.com
 * Date: 14/09/17
 * Time: 9:30 PM
 */

class ShowingTransformerTest extends TestCase
{
    public function testNoShowings() {
        $movie = ['showings' => []];
        $filter = new ShowingTransformer(new \DateTime('12:00'));
        $this->assertNull($filter->transform($movie));
    }
    public function testOneShowings() {
        $movie = ['showings' => [ "19:00:00+11:00"]];
        $filter = new ShowingTransformer(new \DateTime('12:00'));
        $transformed = $filter->transform($movie);
        $movie['showings'] = [ new \DateTime("19:00:00") ];
        $this->assertEquals($movie, $transformed);
    }
    public function testTwoShowingsFirst() {
        $movie = ['showings' => [ "19:00:00+11:00", "21:00:00+11:00"]];
        $filter = new ShowingTransformer(new \DateTime('12:00'));
        $transformed = $filter->transform($movie);
        $movie['showings'] = [ new \DateTime("19:00:00") ];
        $this->assertEquals($movie, $transformed);
    }
    public function testTwoShowingsSecond() {
        $movie = ['showings' => [ "19:00:00+11:00", "21:00:00+11:00"]];
        $filter = new ShowingTransformer(new \DateTime('20:00'));
        $transformed = $filter->transform($movie);
        $movie['showings'] = [ new \DateTime("21:00:00") ];
        $this->assertEquals($movie, $transformed);
    }
    public function testTwoShowingsNeither() {
        $movie = ['showings' => [ "19:00:00+11:00", "21:00:00+11:00"]];
        $filter = new ShowingTransformer(new \DateTime('22:00'));
        $transformed = $filter->transform($movie);
        $movie['showings'] = [ new \DateTime("21:00:00") ];
        $this->assertNull($transformed);
    }
    public function testJustBefore() {
        $movie = ['showings' => [ "19:00:00+11:00", "21:00:00+11:00"]];
        $filter = new ShowingTransformer(new \DateTime('18:59:50'));
        $transformed = $filter->transform($movie);
        $movie['showings'] = [ new \DateTime("19:00:00") ];
        $this->assertEquals($movie, $transformed);
    }
    public function testExactly() {
        $movie = ['showings' => [ "19:00:00+11:00", "21:00:00+11:00"]];
        $filter = new ShowingTransformer(new \DateTime('19:00'));
        $transformed = $filter->transform($movie);
        $movie['showings'] = [ new \DateTime("19:00:00") ];
        $this->assertEquals($movie, $transformed);
    }
    public function testJustAfter() {
        $movie = ['showings' => [ "19:00:00+11:00", "21:00:00+11:00"]];
        $filter = new ShowingTransformer(new \DateTime('19:00:01'));
        $transformed = $filter->transform($movie);
        $movie['showings'] = [ new \DateTime("21:00:00") ];
        $this->assertEquals($movie, $transformed);
    }
}
