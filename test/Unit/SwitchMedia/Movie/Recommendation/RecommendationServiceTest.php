<?php

use GuzzleHttp\Client;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\Psr7\Response;
use SwitchMedia\Movie\Movie;
use SwitchMedia\Movie\Recommendation\RecommendationService;

/**
 * Author: willem.j.homan@gmail.com
 * Date: 14/09/17
 * Time: 4:25 PM
 */
class RecommendationServiceTest extends \PHPUnit\Framework\TestCase
{
    public function testNoMatchGenre()
    {
        $recommendationService = $this->createRecommendationService('arthouse', '12:00');
        $recommendations = $recommendationService->recommend();
        $expected = [];
        $this->assertEquals($expected, $recommendations);
    }
    public function testSingleMatchGenre()
    {
        $recommendationService = $this->createRecommendationService('drama', '12:00');
        $recommendations = $recommendationService->recommend();
        $expected = [
            new Movie("Moonlight", [new \DateTime('18:30:00')], 98),
        ];
        $this->assertEquals($expected, $recommendations);
    }

    public function testMultipleMatchesGenre()
    {
        $recommendationService = $this->createRecommendationService('comedy', '12:00');
        $recommendations = $recommendationService->recommend();
        $expected = [
            new Movie("Zootopia", [new \DateTime('19:00:00')], 92),
            new Movie("Shaun The Sheep", [new \DateTime('19:00:00')], 80)
        ];
        $this->assertEquals($expected, $recommendations);
    }
    public function testNoMatchTime()
    {
        $recommendationService = $this->createRecommendationService('comedy', '23:00');
        $recommendations = $recommendationService->recommend();
        $expected = [];
        $this->assertEquals($expected, $recommendations);
    }
    public function testSingleMatchTime()
    {
        $recommendationService = $this->createRecommendationService('comedy', '20:00');
        $recommendations = $recommendationService->recommend();
        $expected = [
            new Movie("Zootopia", [new \DateTime('21:00:00')], 92),
        ];
        $this->assertEquals($expected, $recommendations);
    }

    public function testMultipleMatchesTime()
    {
        $recommendationService = $this->createRecommendationService('comedy', '12:00');
        $recommendations = $recommendationService->recommend();
        $expected = [
            new Movie("Zootopia", [new \DateTime('19:00:00')], 92),
            new Movie("Shaun The Sheep", [new \DateTime('19:00:00')], 80)
        ];
        $this->assertEquals($expected, $recommendations);
    }
    public function testMultipleTimeJustOverThirtyMinutes()
    {
        $recommendationService = $this->createRecommendationService('comedy', '18:29:59');
        $recommendations = $recommendationService->recommend();
        $expected = [
            new Movie("Zootopia", [new \DateTime('19:00:00')], 92),
            new Movie("Shaun The Sheep", [new \DateTime('19:00:00')], 80)
        ];
        $this->assertEquals($expected, $recommendations);
    }
    public function testMultipleTimeJustUnderThirtyMinutes()
    {
        $recommendationService = $this->createRecommendationService('comedy', '18:30:01');
        $recommendations = $recommendationService->recommend();
        $expected = [
            new Movie("Zootopia", [new \DateTime('21:00:00')], 92),
        ];
        $this->assertEquals($expected, $recommendations);
    }
    public function testMultipleTimeJustExactlyThirtyMinutes()
    {
        $recommendationService = $this->createRecommendationService('comedy', '18:30');
        $recommendations = $recommendationService->recommend();
        $expected = [
            new Movie("Zootopia", [new \DateTime('19:00:00')], 92),
            new Movie("Shaun The Sheep", [new \DateTime('19:00:00')], 80)
        ];
        $this->assertEquals($expected, $recommendations);
    }
    public function testFirstShowingOnly()
    {
        $recommendationService = $this->createRecommendationService('Action & Adventure', '12:30');
        $recommendations = $recommendationService->recommend();
        $expected = [
            new Movie("Zootopia", [new \DateTime('19:00:00')], 92),
        ];
        $this->assertEquals($expected, $recommendations);
    }
    public function testRatingSort()
    {
        $ghostbusters = ['name' => "Ghostbusters", "genres" => ["Comedy", "Drama"], "rating" => 88, "showings"=> [
            "18:30:00+11:00",
            "20:30:00+11:00"
        ]];
        $recommendationService = $this->createRecommendationService('comedy', '12:00', [$ghostbusters]);
        $recommendations = $recommendationService->recommend();
        $expected = [
            new Movie("Zootopia", [new \DateTime('19:00:00')], 92),
            new Movie("Ghostbusters", [new \DateTime('18:30:00')], 88),
            new Movie("Shaun The Sheep", [new \DateTime('19:00:00')], 80)
        ];
        $this->assertEquals($expected, $recommendations);
    }

    /**
     * @param string $genre
     * @param string $time
     * @return RecommendationService
     */
    public function createRecommendationService(string $genre, string $time, $moreMovies=[]): RecommendationService
    {
        global $container;
        $container->set('genre', $genre);
        $container->set('time', $time);
        \SampleData::addMovies($moreMovies);
        // Create a mock and queue a response.
        $mockHandler = new MockHandler([
            new Response(200, ['Content-Type:application/json'], \SampleData::asJson())
        ]);
        $client = new Client(['handler' => $mockHandler]);
        $container->set(ClientInterface::class, $client);
        return $container->make(RecommendationService::class);
    }
}
