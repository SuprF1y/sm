<?php
namespace Unit\SwitchMedia\Movie\Recommendation;
use PHPUnit\Framework\TestCase;
use SwitchMedia\Movie\Recommendation\GenreFilter;

/**
 * Author: willem.j.homan@gmail.com
 * Date: 14/09/17
 * Time: 9:30 PM
 */

class GenreFilterTest extends TestCase
{
    public function testNoGenres() {
        $movie = ['genres' => []];
        $filter = new GenreFilter('comedy');
        $this->assertNull($filter->transform($movie));
    }
    public function testOneGenre() {
        $movie = ['name' => 'Dogs in Space', 'genres' => ['comedy']];
        $filter = new GenreFilter('comedy');
        $this->assertEquals($movie, $filter->transform($movie));
    }
    public function testMultipleGenres() {
        $movie = ['name' => 'Dogs in Space', 'genres' => ['comedy', 'action']];
        $filter = new GenreFilter('comedy');
        $this->assertEquals($movie, $filter->transform($movie));
    }
    public function testNoMatch() {
        $movie = ['name' => 'Dogs in Space', 'genres' => ['comedy', 'action']];
        $filter = new GenreFilter('arthouse');
        $this->assertNull($filter->transform($movie));
    }
    public function caseInsensitive() {
        $movie = ['name' => 'Dogs in Space', 'genres' => ['comedy', 'action']];
        $filter = new GenreFilter('Action');
        $this->assertNull($filter->transform($movie));

        $movie = ['name' => 'Dogs in Space', 'genres' => ['comedy', 'Action']];
        $filter = new GenreFilter('action');
        $this->assertNull($filter->transform($movie));
    }
}
