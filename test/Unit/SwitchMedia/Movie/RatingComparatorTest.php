<?php

namespace Unit\SwitchMedia\Movie;

use PHPUnit\Framework\TestCase;
use SwitchMedia\Movie\Comparator;
use SwitchMedia\Movie\Movie;
use SwitchMedia\Movie\RatingComparator;
use SwitchMedia\Movie\Recommendation\GenreFilter;

/**
 * Author: willem.j.homan@gmail.com
 * Date: 14/09/17
 * Time: 9:30 PM
 */
class RatingComparatorTest extends TestCase
{
    public function testLessThan()
    {
        $movie1 = new Movie('Dogs in Space', ['Cult'], 99);
        $movie2 = new Movie('Titanic', ['Cult'], 1);
        $comparator = new RatingComparator;
        $this->assertEquals(-1, $comparator($movie1, $movie2));
    }
    public function testGreaterThan()
    {
        $movie1 = new Movie('Dogs in Space', ['Cult'], 1);
        $movie2 = new Movie('Titanic', ['Cult'], 99);
        $comparator = new RatingComparator;
        $this->assertEquals(1, $comparator($movie1, $movie2));
    }
    public function testEqual()
    {
        $movie1 = new Movie('Dogs in Space', ['Cult'], 99);
        $movie2 = new Movie('Titanic', ['Cult'], 99);
        $comparator = new RatingComparator;
        $this->assertEquals(0, $comparator($movie1, $movie2));
    }
}
