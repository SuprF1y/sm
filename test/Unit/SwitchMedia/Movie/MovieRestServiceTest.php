<?php
/**
 * Author: willem.j.homan@gmail.com
 * Date: 12/09/17
 * Time: 10:45 PM
 */

namespace Unit\SwitchMedia\Movie;
use SwitchMedia\Movie\Movie;
use GuzzleHttp\Client;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\Psr7\Response;

use SwitchMedia\Movie\MovieFactory;
use SwitchMedia\Movie\MovieRestService;
use PHPUnit\Framework\TestCase;

/**
 * Unit tests for MovieRestService
 * Class MovieRestServiceTest
 * @package Unit\SwitchMedia\Movie
 */
class MovieRestServiceTest extends TestCase
{
    const URL='http://dummy-url/movies/';

    /**
     * @covers MovieRestService::findAll()
     */
    public function testFindAll() {
        // Create a mock and queue a response.
        $mockHandler = new MockHandler([
            new Response(200, ['Content-Type:application/json'], \SampleData::asJson())
        ]);

        $service = new MovieRestService(self::URL, new Client(['handler' => $mockHandler]));
        $movies = $service->findAll();

        $this->assertCount(4 , $movies);
        $this->assertEquals($movies, \SampleData::asArray());
    }
    /**
     * @expectedException \SwitchMedia\Exception\ServiceException
     * @expectedExceptionMessage Could not decode response body or empty response body
     */
    public function testEmptyResponseBody()
    {
        // Create a mock and queue a response.
        $mockHandler = new MockHandler([
            new Response(200, ['Content-Type:application/json'], '')
        ]);

        $service = new MovieRestService(self::URL, new Client(['handler' => $mockHandler]));
        $service->findAll();

    }

    public function testNoMovies()
    {
        // Create a mock and queue a response.
        $mockHandler = new MockHandler([
            new Response(200, ['Content-Type:application/json'], '[]')
        ]);

        $service = new MovieRestService(self::URL, new Client(['handler' => $mockHandler]));
        $movies = $service->findAll();
        $this->assertEquals([], $movies);

    }
    /**
     * @expectedException \SwitchMedia\Exception\ServiceException
     * @expectedExceptionMessage Bad status code: 503
     */
    public function testServerError()
    {
        // Create a mock and queue a response.
        $mockHandler = new MockHandler([
            new Response(503, ['Content-Type:application/json'], '[]')
        ]);

        $service = new MovieRestService(self::URL, new Client(['handler' => $mockHandler]));
        $service->findAll();

    }

}
