<?php

use SwitchMedia\Movie\MovieFactory;

/**
 * Author: willem.j.homan@gmail.com
 * Date: 14/09/17
 * Time: 7:26 PM
 */


class SampleData
{
    public static function asArray()
    {
        return json_decode(self::$sample, true);
    }

    public static function asJson()
    {
        return self::$sample;
    }

    public static function asMovies()
    {
        $movieFactory = new MovieFactory();
        $movies =  array_map([$movieFactory, 'transform'], static::asArray());
        return $movies;
    }
    public static function addMovie(array $movie){
        $movies = self::asArray();
        $movies[] = $movie;
        static::$sample = json_encode($movies);
    }
    public static function addMovies(array $movies){
        foreach($movies as $movie){
            self::addMovie($movie);
        }
    }
    protected static $sample = '[{
        "name": "Moonlight",
        "rating": 98,
        "genres": [
            "Drama"
        ],
        "showings": [
            "18:30:00+11:00",
            "20:30:00+11:00"
        ]
    },
    {
        "name": "Zootopia",
        "rating": 92,
        "genres": [
            "Action & Adventure",
            "Animation",
            "Comedy"
        ],
        "showings": [
            "19:00:00+11:00",
            "21:00:00+11:00"
        ]
    },
    {
        "name": "The Martian",
        "rating": 92,
        "genres": [
            "Science Fiction & Fantasy"
        ],
        "showings": [
            "17:30:00+11:00",
            "19:30:00+11:00"
        ]
    },
    {
        "name": "Shaun The Sheep",
        "rating": 80,
        "genres": [
            "Animation",
            "Comedy"
        ],
        "showings": [
            "19:00:00+11:00"
        ]
    }]';
}
