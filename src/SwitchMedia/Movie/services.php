<?php
/**
 * DI service definitions for the MovieService
 * Author: willem.j.homan@gmail.com
 * Date: 13/09/17
 * Time: 11:05 AM
 */
use SwitchMedia\Exception\ConfigurationException;
use SwitchMedia\Movie\MovieFactory;
use SwitchMedia\Movie\MovieRestService;
use SwitchMedia\Movie\MovieService;
use DI\Scope;
use GuzzleHttp\Client;
use GuzzleHttp\ClientInterface;

return [
    MovieFactory::class,

    // guzzle client
    ClientInterface::class =>
        DI\object(Client::class)
            ->scope(Scope::PROTOTYPE),

    // MovieService
    MovieService::class => DI\Factory(function (ClientInterface $client) {
        $scheme = getEnvVar('MOVIE_SERVER_SCHEME');
        $host = getEnvVar('MOVIE_SERVER_HOST');
        $port = getEnvVar('MOVIE_SERVER_PORT', false);
        if ( isset($port) ){
            $host .= ":$port";
        }
        $basePath = getEnvVar('MOVIE_BASE_PATH');
        $url = $scheme . "://" . $host .  $basePath;

        return new MovieRestService($url, $client);
    })->scope(Scope::PROTOTYPE)
];

/**
 * loads an environment variable
 * @param string $name
 * @param bool $isRequired
 * @return null|string
 * @throws ConfigurationException if env var is not set in the environment
 */
function getEnvVar(string $name, bool $isRequired=true): ?string
{
    $envVar = getenv($name);
    if ($isRequired && $envVar === false) {
        throw new ConfigurationException("Movie service has requires env var $name to be set");
    }
    return $envVar;
}
