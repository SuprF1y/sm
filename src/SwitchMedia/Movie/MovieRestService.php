<?php
/**
 * Author: willem.j.homan@gmail.com
 * Date: 12/09/17
 * Time: 9:41 PM
 */

namespace SwitchMedia\Movie;

use SwitchMedia\Exception\ServiceException;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\ConnectException;
use Psr\Http\Message\ResponseInterface;
use Teapot\StatusCode;

/**
 * REST Implementation of MovieService
 * Class MovieRestService
 * @package SwitchMedia\Movie
 */
class MovieRestService implements MovieService
{
    /**
     * @var string
     */
    private $url;
    /**
     * @var ClientInterface
     */
    private $client;

    public function __construct(string $url, ClientInterface $client)
    {
        $this->url = $url;
        $this->client = $client;
    }

    /**
     * @inheritdoc
     */
    public function findAll(): array
    {

        try {
            $response = $this->client->request('GET', $this->url, []);
        } catch (ConnectException $e) {
            throw new ServiceException("Could not load Movies, could not connect to Service endpoint:" . $this->url);
        }
        return $this->parseResponse($response);
    }


    /**
     * Decode the guzzle response, throwing exceptions if it is not what is expected
     * @param ResponseInterface $response
     * @return array
     */
    protected function parseResponse(ResponseInterface $response): array
    {
        // make sure we get a 200 response
        if ($response->getStatusCode() !== StatusCode::OK) {
            $this->reportError($response, "Bad status code: " . $response->getStatusCode());
        }

        // make sure we have a none empty valid body
        $movieData = json_decode($response->getBody(), true);
        if ($movieData === null) {
            $this->reportError($response, "Could not decode response body or empty response body");
        }

        return $movieData;
    }

    protected function reportError(ResponseInterface $response, $reason)
    {
        throw ServiceException::fromResponse("Could not load Movies: $reason", $response);
    }
}
