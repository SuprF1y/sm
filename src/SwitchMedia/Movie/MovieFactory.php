<?php
/**
 * Author: willem.j.homan@gmail.com
 * Date: 14/09/17
 * Time: 3:21 PM
 */

namespace SwitchMedia\Movie;


/**
 * Constructs a Movie from a movie array representation
 * Class MovieFactory
 * @package SwitchMedia\Movie
 */
class MovieFactory implements Transformer
{
    public function transform(array $movie)
    {
        $showings = $this->createShowings($movie['showings']);
        return new Movie($movie['name'], $showings, $movie['rating']);
    }

    protected function createShowings(array $showings)
    {
        return array_map(function ($showing) {
            if (! $showing instanceof \DateTime) {
                $showing = new \DateTime($showing);
            }
            return $showing;
        },
            $showings
        );
    }
}
