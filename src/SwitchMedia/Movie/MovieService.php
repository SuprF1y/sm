<?php
/**
 * Author: willem.j.homan@gmail.com
 * Date: 13/09/17
 * Time: 12:50 PM
 */

namespace SwitchMedia\Movie;

/**
 * interface defining API for MovieService implementations
 * Interface MovieService
 * @package SwitchMedia\Movie
 */
interface  MovieService
{
    /**
     * Find all Movies
     * @return array
     */
    public function findAll(): array;

}
