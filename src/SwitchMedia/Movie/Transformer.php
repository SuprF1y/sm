<?php
/**
 * Author: willem.j.homan@gmail.com
 * Date: 14/09/17
 * Time: 3:50 PM
 */

namespace SwitchMedia\Movie;


/**
 * takes a movie represented as an assoc array and transforms it
 * Interface Transformer
 * @package SwitchMedia\Movie
 */
interface Transformer
{
    /**
     * transform the movie array
     * @param array $movie
     * @return mixed the transformed movie or null
     */
    public function transform(array $movie);
}
