<?php
/**
 * Author: willem.j.homan@gmail.com
 * Date: 14/09/17
 * Time: 4:00 PM
 */

namespace SwitchMedia\Movie;


/**
 * Chains a series of transformers together
 * Class TransformerChain
 * @package SwitchMedia\Movie
 */
class TransformerChain implements Transformer
{
    /**
     * @var Transformer[]
     */
    protected $transformers;
    /**
     * TransformerChain constructor.
     * @param Transformer[] ...$transformers
     */
    public function __construct(...$transformers){
        $this->transformers = $transformers;
    }

    /**
     * Passes the movie to each transformer in turn. If any of the transformers return null, then entire chain returns null
     * @param array $movie
     * @return array|null there transformed movie or null
     */
    public function transform(array $movie){
        foreach($this->transformers as $transformer ){
            $movie = $transformer->transform($movie);
            if ( $movie === null ){
                return null;
            }
        }
        return $movie;
    }
}
