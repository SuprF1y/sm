<?php
/**
 * Author: willem.j.homan@gmail.com
 * Date: 14/09/17
 * Time: 8:06 PM
 */

namespace SwitchMedia\Movie;

/**
 * comparator Invokable for sorting by movie ration
 * Class RatingComparator
 * @package SwitchMedia\Movie
 */
class RatingComparator implements Comparator
{

    /**
     * @param Movie $movie1
     * @param Movie $movie2
     * @return int
     */
    public function __invoke(Movie $movie1, Movie $movie2): int
    {
        return $movie2->getRating() <=> $movie1->getRating();
    }
}
