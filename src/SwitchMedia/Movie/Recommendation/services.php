<?php
/**
 * DI service definitions for the MovieService
 * Author: willem.j.homan@gmail.com
 * Date: 13/09/17
 * Time: 11:05 AM
 */

use DI\Scope;
use GuzzleHttp\Client;
use GuzzleHttp\ClientInterface;
use SwitchMedia\Movie\MovieFactory;
use SwitchMedia\Movie\RatingComparator;
use SwitchMedia\Movie\Recommendation\GenreFilter;
use SwitchMedia\Movie\Recommendation\RecommendationService;
use SwitchMedia\Movie\Recommendation\ShowingTransformer;
use SwitchMedia\Movie\Comparator;
use SwitchMedia\Movie\Transformer;
use SwitchMedia\Movie\TransformerChain;

return [
    // the main recommendation service class
    RecommendationService::class  => DI\object(RecommendationService::class)->scope(Scope::PROTOTYPE),

    // Rating Comparator
    Comparator::class => DI\object(RatingComparator::class),

    // MovieTransformer chain to filter and transform movies
    Transformer::class => DI\Factory(function (string $genre, string $time) {
        // movies must start at least 30 mins after supplied time
        $dateTime = new \DateTime("$time +30 mins");
        return new TransformerChain(
            // filter by genre
            new GenreFilter($genre),
            // get movies showing only 30 mins after start time, and retain only the soonest showtime for these movies
            new ShowingTransformer($dateTime),
            // create DTO's from array representation of movies
            new MovieFactory()
        );
    })->parameter('genre', DI\get('genre'))
        ->parameter('time', DI\get('time'))
        ->scope(Scope::PROTOTYPE)
];

