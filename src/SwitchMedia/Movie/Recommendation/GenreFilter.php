<?php
/**
 * Author: willem.j.homan@gmail.com
 * Date: 14/09/17
 * Time: 3:53 PM
 */

namespace SwitchMedia\Movie\Recommendation;


use SwitchMedia\Movie\Transformer;

/**
 * returns the supplied movie unchanged if movie is in genre, or null if it is not
 * Class GenreFilter
 * @package SwitchMedia\Movie\Recommendation
 */
class GenreFilter implements Transformer
{
    /**
     * @var string
     */
    private $genre;

    /**
     * GenreFilter constructor.
     * @param string $genre
     */
    public function __construct(string $genre)
    {

        $this->genre = $genre;
    }


    /**
     * @param array $movie
     * @return array
     */
    public function transform(array $movie)
    {
        if (in_array(strtolower($this->genre), array_map('strtolower', $movie['genres']))) {
            return $movie;
        } else {
            return null;
        }
    }
}
