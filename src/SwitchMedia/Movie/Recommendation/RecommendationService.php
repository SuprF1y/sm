<?php
/**
 * Author: willem.j.homan@gmail.com
 * Date: 14/09/17
 * Time: 4:08 PM
 */

namespace SwitchMedia\Movie\Recommendation;


use SwitchMedia\Movie\MovieService;
use SwitchMedia\Movie\Comparator;
use SwitchMedia\Movie\Transformer;

class RecommendationService
{
    /**
     * @var MovieService
     */
    private $movieService;
    /**
     * @var Transformer
     */
    private $transformer;
    /**
     * @var Comparator
     */
    private $comparator;

    public function __construct(MovieService $movieService, Transformer $transformer, Comparator $comparator)
    {
        $this->movieService = $movieService;
        $this->transformer = $transformer;
        $this->comparator = $comparator;
    }

    public function recommend()
    {
        // get all the movies
        $movies = $this->movieService->findAll();
        $recommendations = [];

        // for all movies
        foreach($movies as $movie){
            // run them through the transformer/filter chain
            $transformed = $this->transformer->transform($movie);
            // if we get a match then add it to the list of the recommendations
            if( $transformed != null){
                $recommendations[] = $transformed ;
            }
        };
        // sort the recommendations
        usort($recommendations, $this->comparator);
        return $recommendations;
    }
}
