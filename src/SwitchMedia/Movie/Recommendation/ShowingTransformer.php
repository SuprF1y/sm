<?php
/**
 * Author: willem.j.homan@gmail.com
 * Date: 14/09/17
 * Time: 3:53 PM
 */

namespace SwitchMedia\Movie\Recommendation;


use DateTime;
use SwitchMedia\Exception\ServiceException;
use SwitchMedia\Movie\Transformer;

/**
 * returns a movie with the showing soonest to passed in DateTime
 * if no movies are after the DateTime returns null
 * Class ShowingTransformer
 * @package SwitchMedia\Movie\Recommendation
 */
class ShowingTransformer implements Transformer
{
    /**
     * @var \DateTime
     */
    private $dateTime;

    public function __construct(\DateTime $dateTime)
    {
        $this->dateTime = $dateTime;
    }

    /**
     * @param array $movie
     * @return array
     */
    public function transform(array $movie): ?array
    {
        $showings = $this->getShowing($movie['showings']);
        if (empty($showings)) {
            return null;
        } else {
            $movie['showings'] = [$showings];
        }
        return $movie;
    }

    /**
     * return a single showing which is airing soonest to the datetime or null if no showing later than datetime
     * @param array $showings
     * @return DateTime|null
     * @throws ServiceException
     */
    protected function getShowing(array $showings):?DateTime
    {
        $earliest = null;
        foreach ($showings as $showing) {
            $match = [];
            // discard the timezone offset
            $res = preg_match('|(\d\d:\d\d:\d\d)|', $showing, $match);
            if ( $res != 1){
                throw new ServiceException("unexected time format for showing : $showing");
            }
            $showing = new \DateTime($match[0]);
            // this showing is later than the cutoff time
            if ($showing >= $this->dateTime) {
                // set earliest if we don't already have one or if this showing is earlier than the current earliest
                if ( $earliest === null || $showing < $earliest){
                    $earliest =  $showing;
                }
            }
        }
        return $earliest;
    }
}
