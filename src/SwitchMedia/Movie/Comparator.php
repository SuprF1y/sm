<?php
/**
 * Author: willem.j.homan@gmail.com
 * Date: 14/09/17
 * Time: 8:04 PM
 */

namespace SwitchMedia\Movie;

/**
 * Compares two movies
 * Interface Comparator
 * @package SwitchMedia\Movie
 */
interface Comparator
{
    /**
     * compares two Movies
     * @param Movie $movie1
     * @param Movie $movie2
     * @return int -1 , 0 or 1
     */
    public function __invoke(Movie $movie1, Movie $movie2): int;
}
