<?php
/**
 * Author: willem.j.homan@gmail.com
 * Date: 13/09/17
 * Time: 12:10 PM
 */

namespace SwitchMedia\Movie;

use DateTime;

/**
 * Movie DTO
 * Class Movie
 * @package SwitchMedia\Movie
 */
class Movie
{

    /**
     * @var string
     */
    private $name;
    /**
     * @var DateTime[]
     */
    private $showings;
    /**
     * @var int
     */
    private $rating;


    public function __construct(string $name, array $showings, int $rating)
    {
        $this->name = $name;
        $this->showings = $showings;
        $this->rating = $rating;
    }

    /**
     * @return DateTime[]
     */
    public function getShowings(): array
    {
        return $this->showings;
    }


    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return int
     */
    public function getRating(): int
    {
        return $this->rating;
    }

    public function __toString()
    {
        $time = $this->getFirstShowing()->format("g:ia");
        $time = preg_replace("|:00|", "", $time);
        return $this->getName() .  ", showing at " . $time;
    }

    /**
     * returns the first showing in the list of showings
     * @return DateTime
     */
    protected function getFirstShowing(): DateTime
    {
        if(count($this->showings) < 1){
            throw new \LogicException("No showings for movie " . $this->name);
        }
        return $this->showings[0];
    }


}
