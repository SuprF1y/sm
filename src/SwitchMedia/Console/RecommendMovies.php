<?php
/**
 * Author: willem.j.homan@gmail.com
 * Date: 14/09/17
 * Time: 8:33 PM
 */
namespace SwitchMedia\Console;
use SwitchMedia\Movie\Recommendation\RecommendationService;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Symfony Console command for recommending movies airing at least 30 minutes after the supplied time and in supplied genre
 * Class RecommendMovies
 * @package SwitchMedia\Console
 */
class RecommendMovies extends Command
{
    protected function configure()
    {
        $this
            ->setName('movie:recommend')

            // the short description shown while running "php bin/console list"
            ->setDescription('Recommend movies')

            // the full command description shown when running the command with
            // the "--help" option
            ->setHelp('This command will recommend movies in the supplied genre which starting at least 30 after the time provied ')

            ->addArgument('genre', InputArgument::REQUIRED, 'genre to search for')
            ->addArgument('time', InputArgument::REQUIRED, 'movies will start at least 30 mins after this time. 24 hr time format');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $genre = $input->getArgument('genre');
        $time = $input->getArgument('time');
        if ( preg_match("|^\d\d:\d\d$|", $time) != 1){
            $output->writeln("<error>Error: time must be 24 hour hh:mm format e.g. 14:06</error>");
            die(-1);
        }
        global $container;
        $container->set('genre', $genre);
        $container->set('time', $time);

        $recommendationService = $container->make(RecommendationService::class);
        $recommendations = $recommendationService->recommend();
        if (empty($recommendations)){
            $recommendations = "no movie recommendations";
        }
        $output->writeln($recommendations);
    }
}
