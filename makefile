.phony: up down restart clean tests

clean: down

install:

up:
	docker-compose up

tests:
	docker-compose run --user interview php vendor/bin/phpunit  --bootstrap ./bootstrap.php --configuration test/phpunit.xml

down:
	docker-compose stop

bash:
	docker-compose run php bash

restart: down up

world: clean install up

build:
	docker-compose build php

autoload:
	docker-compose run --user interview php composer dump-autoload


composer:
	docker-compose run --user interview php composer require --dev mockery/mockery
	docker-compose run --user interview php composer require --dev phpunit/phpunit
	docker-compose run --user interview php composer require guzzlehttp/guzzle
	docker-compose run --user interview php composer require php-di/php-di
	docker-compose run --user interview php composer require vlucas/phpdotenv
	docker-compose run --user interview php composer require shrikeh/teapot

console:
	docker-compose run --user interview php composer require symfony/console


